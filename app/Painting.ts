class Painting{
	
	title:string; // Painting Title
	path:string;
	
	constructor(title:string, path:string){
		this.title = title;	
		this.path = path;
	}
	
	render():string{
		
		return '<div class="col-md-4">' +
                  '<div class="thumbnail">' +
                      '<img src="'+this.path+'" alt="...">' +
    	              '<div class="caption">' +
                        '<p>'+this.title+'</p>' +
                      '</div>' +
                    '</div>' +
                '</div>'
		
	}
	
}