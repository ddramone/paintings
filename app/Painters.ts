/// <reference path="../typings/tsd.d.ts" />

interface IPainter {
	
	name: string;
	style: string;
	birth: Date;
	death: Date;
	
	biography: string;
	
	works?: Painting[];
	
}


class Painter implements IPainter{
	
	public name: string;
	public style: string;
	public birth: Date;
	public death: Date;
	
	public biography: string;
	
	public works: Painting[] = [];

	/**
	 * @constructor
	 */	
	constructor(info:IPainter){
		
		this.name = info.name;
		this.style = info.style;
		this.birth = new Date(Date.parse(info.birth.toString()));
		this.death = new Date(Date.parse(info.death.toString()));
		
		this.biography = info.biography;
		
		//Loop works and save to this.works sthore
		for(var i in info.works){
			var pinfo = <Object>info.works[i];
			
			var painting  = new Painting(pinfo['name'], pinfo['media']);
			
			this.works.push(painting);
				
		}
		
	}
	
	/**
	 * Renders current painter into given html element
	 */
	renderMe(target: HTMLElement):void{
		
		var monthNames:string[] = ["January", "February", "March","April", "May", "June", "July","August", "September", "October","November", "December"];

		var formattedBirthDate: string =  this.birth.getDate() + ' ' +monthNames[this.birth.getMonth()] + ' ' +this.birth.getFullYear();
		var formattedDeathDate: string =  this.death.getDate() + ' ' +monthNames[this.death.getMonth()] + ' ' +this.death.getFullYear();
	
		var html:string[] = [];
		
		html.push("<h1>"+this.name+" <small>"+this.style+"</small></h1>");
		
		html.push("<p>Birth: <em>"+formattedBirthDate+"</em>, Death: <em>"+formattedDeathDate+"</em></p>");
		
		html.push("<h3>Examples</h3>");
		
		for(var p in this.works){
			var painting:Painting = this.works[p];
			
			html.push(painting.render());
			
		}
						
		html.push("<h3>Short Biograiphy</h3>");
		html.push(this.biography);
		
		target.innerHTML = html.join('');
	}
	
}