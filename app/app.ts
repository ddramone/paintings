﻿//Add your initialization logic here

class App {


    public painters: Painter[] = []; //Painters Store
    
    private _paintersList: HTMLElement;
    private _painterInfo: HTMLElement;
    
    /**
     * @constructor
     */
    constructor() {

        var me = this;
        this._paintersList = <HTMLElement>document.getElementById("PaintersList");
        this._painterInfo = <HTMLElement>document.getElementById("PainterInfo");

        this.loadJSON();
        
        //Add Listeners
        this._paintersList.addEventListener('click', function(event: Event) {

            var target: Element = (<Element>event.target);
            //If Event Target is "a" tag (name of painter)
            if (target.tagName.toLowerCase() == 'a') {
                
                var index:number,
                    painter:Painter;
                
                //Take index of painter
                index = parseInt(target.getAttribute("data-index"));
                //Take painter from store by index
                painter = me.painters[index];
                
                //Render painter
                painter.renderMe(me._painterInfo);
                
            }
        });

    }//end
    
    
    renderPainters(): void {

        var i, painter: Painter,
            html: string[] = [];

        for (i in this.painters) {

            painter = this.painters[i];
            html.push('<li><a data-index="' + i + '" href="#">' + painter.name + '</a></li>');

        }

        this._paintersList.innerHTML = html.join('');

    }//end
    
    /**
     * Load Json Data
     * Store painters into App.painters
     */
    loadJSON(): void {
        
        //localize
        var me = this,
            request: XMLHttpRequest = new XMLHttpRequest();

        request.onload = function() {

            var painters = JSON.parse(this.responseText);


            for (var i in painters.famousPainters) {
                var info = painters.famousPainters[i];

                
                var painter = new Painter({
                    name: info.name,
                    style: info.style,
                    birth: info.birth_date,
                    death: info.death_date,
                    biography: info.bio,
                    
                    works: info.examples
                    
                });

                me.painters.push(painter);

            }
            
            //Render Painters as soon as they are loaded
            me.renderPainters();

        };

        request.open("get", "JSON/famousPainters.json", true);
        request.send();

    }//end
    
}

(function() {

    var app = new App();

})();


