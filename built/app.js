/// <reference path="../typings/tsd.d.ts" />
var Painter = (function () {
    function Painter(info) {
        this.works = [];
        this.name = info.name;
        this.style = info.style;
        this.birth = new Date(Date.parse(info.birth.toString()));
        this.death = new Date(Date.parse(info.death.toString()));
        this.biography = info.biography;
        for (var i in info.works) {
            var pinfo = info.works[i];
            var painting = new Painting(pinfo['name'], pinfo['media']);
            this.works.push(painting);
        }
    }
    Painter.prototype.renderMe = function (target) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var formattedBirthDate = this.birth.getDate() + ' ' + monthNames[this.birth.getMonth()] + ' ' + this.birth.getFullYear();
        var formattedDeathDate = this.death.getDate() + ' ' + monthNames[this.death.getMonth()] + ' ' + this.death.getFullYear();
        var html = [];
        html.push("<h1>" + this.name + " <small>" + this.style + "</small></h1>");
        html.push("<p>Birth: <em>" + formattedBirthDate + "</em>, Death: <em>" + formattedDeathDate + "</em></p>");
        html.push("<h3>Examples</h3>");
        for (var p in this.works) {
            var painting = this.works[p];
            html.push(painting.render());
        }
        html.push("<h3>Short Biograiphy</h3>");
        html.push(this.biography);
        target.innerHTML = html.join('');
    };
    return Painter;
})();
var Painting = (function () {
    function Painting(title, path) {
        this.title = title;
        this.path = path;
    }
    Painting.prototype.render = function () {
        return '<div class="col-md-4">' +
            '<div class="thumbnail">' +
            '<img src="' + this.path + '" alt="...">' +
            '<div class="caption">' +
            '<p>' + this.title + '</p>' +
            '</div>' +
            '</div>' +
            '</div>';
    };
    return Painting;
})();
//Add your initialization logic here
var App = (function () {
    function App() {
        this.painters = [];
        var me = this;
        this._paintersList = document.getElementById("PaintersList");
        this._painterInfo = document.getElementById("PainterInfo");
        this.loadJSON();
        this._paintersList.addEventListener('click', function (event) {
            var target = event.target;
            if (target.tagName.toLowerCase() == 'a') {
                var index, painter;
                index = parseInt(target.getAttribute("data-index"));
                painter = me.painters[index];
                painter.renderMe(me._painterInfo);
            }
        });
    }
    App.prototype.renderPainters = function () {
        var i, painter, html = [];
        for (i in this.painters) {
            painter = this.painters[i];
            html.push('<li><a data-index="' + i + '" href="#">' + painter.name + '</a></li>');
        }
        this._paintersList.innerHTML = html.join('');
    };
    App.prototype.loadJSON = function () {
        var me = this, request = new XMLHttpRequest();
        request.onload = function () {
            var painters = JSON.parse(this.responseText);
            for (var i in painters.famousPainters) {
                var info = painters.famousPainters[i];
                var painter = new Painter({
                    name: info.name,
                    style: info.style,
                    birth: info.birth_date,
                    death: info.death_date,
                    biography: info.bio,
                    works: info.examples
                });
                me.painters.push(painter);
            }
            me.renderPainters();
        };
        request.open("get", "JSON/famousPainters.json", true);
        request.send();
    };
    return App;
})();
(function () {
    var app = new App();
})();
//# sourceMappingURL=app.js.map